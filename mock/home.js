export default {
  'post /api/commons/citys': (req, res) => {
    res.json({
      status: 200,
      data: [[{ label: '杭州', value: '10001' }, { label: '苏州', value: '10002' }]]
    });
  },
  'post /api/house/hot': (req, res) => {
    res.json({
      status: 200,
      data: [
        {
          id: 1,
          img: 'https://hellorfimg.zcool.cn/provider_image/large/2236573390.jpg',
          title: '东城民宿',
          info: '东城区交通方便',
          price: '100'
        },
        {
          id: 2,
          img: 'https://hellorfss.zcool.cn/image-stock/shutterstock/photos/548927521/display_1500/stock-photo-548927521.jpg?auth_key=1638204170-0-0-e39e6e96deac28bc39b8739473562364',
          title: '西城民宿',
          info: '西城区山水怡情',
          price: '120'
        },
        {
          id: 3,
          img: 'https://hellorfss.zcool.cn/image-stock/shutterstock/photos/756956185/display_1500/stock-photo-756956185.jpg?auth_key=1638204195-0-0-06fb43c4976b75d95f2c8fd383f1a482',
          title: '新区民宿',
          info: '新区民宿位置优越',
          price: '200'
        },
        {
          id: 4,
          img: 'https://hellorfimg.zcool.cn/provider_image/large/2236573390.jpg',
          title: '老城民宿',
          info: '老城区风景秀美',
          price: '220'
        }
      ]
    });
  }
};