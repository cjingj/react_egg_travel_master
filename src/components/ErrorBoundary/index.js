import React, { Component } from 'react';
import './index.less';

export default class ErrorBoundary extends Component {

  constructor(props) {
    
    super(props);
    this.state = {
      flag: false
    };
  }

  static getDerivedStateFromError(error) {
    return {
      flag: true
    }
  }
  componentDidCatch(error, info) {
    
  }

  render() {
    return (
      <div>
        {this.state.flag ? <h1 className='mk-error-page'>网络异常，请稍后再试！</h1> : this.props.children}
      </div>
    )
  }
}