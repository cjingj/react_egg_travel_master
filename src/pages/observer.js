import React, { useState, useEffect } from 'react';
import { history } from 'umi';
import { useObserverHook } from '@/hooks';

let observer;
export default function (props) {
    const [state, setState] = useState()

      useObserverHook('#loading', (entries)=>{
      });

      const handleClick = () => {
        history.push('/');
      };

    // useEffect(() => {
    //     console.log('进入页面')
    //     observer = new IntersectionObserver(entries => {
    //         console.log(entries)
    //         //time：可见性发生变化的时间，是一个高精度时间戳，单位为毫秒
    //         // target：被观察的目标元素，是一个 DOM 节点对象
    //         // rootBounds：根元素的矩形区域的信息，getBoundingClientRect()方法的返回值，如果没有根元素（即直接相对于视口滚动），则返回null
    //         // boundingClientRect：目标元素的矩形区域的信息
    //         // intersectionRect：目标元素与视口（或根元素）的交叉区域的信息
    //         //intersectionRatio：目标元素的可见比例，即intersectionRect占boundingClientRect的比例，完全可见时为1，完全不可见时小于等于0
    //         //isIntersecting 是否进入了可视区域
    //     });
    //     observer.observe(document.querySelector('#loading'));

    //     return () => {
    //         console.log('离开页面')
    //         if (observer) {
    //             // 解绑元素
    //             observer.unobserve(document.querySelector('#loading'));

    //             // 停止监听
    //             observer.disconnect();
    //         }
    //     }
    // }, [])

    return (
        <div>
            observer
            <button onClick={handleClick}>首页</button>
            <div id='loading' style={{ width: '100px', height: '100px', background: '#f60', marginTop: '1000px' }}>
                loading
            </div>
        </div>
    )
}