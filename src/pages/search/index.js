import React, { useState, useEffect } from 'react';
import { SearchBar, ActivityIndicator } from 'antd-mobile';
import { useHttpHook, useObserverHook, useImgHook } from '@/hooks';
import { ShowLoading } from '@/components';
import { useLocation } from 'umi';
import { CommonEnum } from '@/enums';
import { history } from 'umi';
import './index.less';
export default function (props) {
  const { query } = useLocation();
  //作为每次修改保存的数据
  const [houseName, setHouseName] = useState('');
  //作为监听发送请求的数据，因为修改了不代表立即发送请求
  const [houseSubmitName, setHouseSubmitName] = useState('');
  const [houseLists, setHouseLists] = useState([]);
  const [showLoading, setShowLoading] = useState(true);
  const [page, setPage] = useState(CommonEnum.PAGE);
  const [houses, loading] = useHttpHook({
    url: '/house/search',
    body: {
      ...page,
      code: query?.code, //url传过来城市的编码
      houseName,
      startTime: query?.startTime + ' 00:00:00',
      endTime: query?.endTime + ' 23:59:59',
    },
    watch: [page.pageNum, houseSubmitName],
  });
  useImgHook('.item-img', (enties) => {}, null);
  /**
   * 1，监听loading是否展示出来；
   * 2，修改分页数据；
   * 3，监听分页数据的修改，发送接口，请求下一页的数据；
   * 4，监听loading变化，拼装数据
   */
  useObserverHook(
    '#' + CommonEnum.LOADING_ID,
    (entries) => {
      if (!loading && entries[0].isIntersecting) {
        setPage({
          ...page,
          pageNum: page.pageNum + 1,
        });
      }
    },
    null,
  );
  //这里因为setHouseSubmitName导致发送请求
  const _handleSubmit = (value) => {
    setHouseName(value);
    setHouseSubmitName(value);
    setPage(CommonEnum.PAGE);
    setHouseLists([]);
  };
  const handleChange = (value) => {
    setHouseName(value);
  };
  const handleSubmit = (value) => {
    _handleSubmit(value);
  };
  const handleCancle = () => {
    _handleSubmit('');
  };
  useEffect(() => {
    if (!loading && houses) {
      if (houses.length) {
        setHouseLists([...houseLists, ...houses]);
        //最后一页的情况
        if (houses.length < page.pageSize) {
          setShowLoading(false);
        }
      } else {
        setShowLoading(false);
      }
    }
  }, [loading]);
  return (
    <div className="search-page">
      {/**顶部搜索栏 */}
      <SearchBar
        placeholder="搜索民宿"
        value={houseName}
        onChange={handleChange}
        onCancel={handleCancle}
        onSubmit={handleSubmit}
      />
      {/**搜索结果 */}
      {loading ? (
        <ActivityIndicator toast />
      ) : (
        <div className="result">
          {houseLists.map((item) => (
            <div
              className="item"
              key={item.id}
              onClick={() => {
                history.push({
                  pathname: '/house',
                  query: {
                    id: item.id,
                  },
                });
              }}
            >
              <img
                alt="img"
                className="item-img"
                src={require('../../assets/blank.png')}
                data-src={item?.imgs[0]?.url}
              />
              <div className="item-right">
                <div className="title">{item.name}</div>
                <div className="price">{item.price}</div>
              </div>
            </div>
          ))}
          <ShowLoading showLoading={showLoading} />
        </div>
      )}
    </div>
  );
}
