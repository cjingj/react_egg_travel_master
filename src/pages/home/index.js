import './index.less';
import React, { useState, useEffect } from 'react';
import Header from './Header';
import Search from './Search';
import Hot from './Hot';
import { useHttpHook } from '@/hooks';
import { ErrorBoundary } from '@/components';
export default function () {
  const [citys, citysLoading] = useHttpHook({
    url: '/commons/citys',
  });
  const [houses] = useHttpHook({
    url: '/house/hot',
  });
  return (
    <ErrorBoundary>
      <div className="home">
        <Header />
        {citys && <Search citys={citys} citysLoading={citysLoading} />}
        {houses && <Hot houses={houses} />}
      </div>
    </ErrorBoundary>
  );
}
