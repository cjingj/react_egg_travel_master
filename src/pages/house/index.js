import React, { useState, useEffect } from 'react';
import Banner from './Banner';
import Info from './Info';
import Lists from './Lists';
import Footer from './Footer';
import { CommonEnum } from '@/enums';
import { useLocation } from 'umi';
import { useStoreHook } from 'think-react-store';
import { useObserverHook } from '@/hooks';
import './index.less';
export default function (props) {
  const {
    house: {
      detail,
      getDetailAsync,
      getCommentsAsync,
      comments,
      reloadComments,
      reloadCommentsNum,
      showLoading,
      resetData,
      order,
      hasOrderAsync,
      addOrderAsync,
      delOrderAsync,
    },
  } = useStoreHook();
  const { query } = useLocation();
  useObserverHook(
    '#' + CommonEnum.LOADING_ID,
    (entries) => {
      if (
        comments &&
        comments.length &&
        showLoading &&
        entries[0].isIntersecting
      ) {
        reloadComments(); //出发reloadCommentsNum修改执行 getCommentsAsync
      }
    },
    [comments, showLoading],
  );
  useEffect(() => {
    getDetailAsync({
      id: query?.id,
    });
  }, []);
  //query house的Id
  useEffect(() => {
    getCommentsAsync({
      id: query?.id,
    });
  }, [reloadCommentsNum]);
  //根据houseId 查看当前house是否被预定
  useEffect(() => {
    hasOrderAsync({
      id: query?.id,
    });
  }, []);

  useEffect(() => {
    return () => {
      resetData({
        detail: {},
      });
    };
  }, []);
  const handleBtnClick = (id) => {
    if (!id) {
      addOrderAsync({
        id: query?.id,
      });
    } else {
      delOrderAsync({
        id,
      });
    }
  };
  return (
    <div className="house-page">
      {/**banner */}
      <Banner banner={detail?.banner} />
      {/**房屋信息 */}
      <Info detail={detail?.info} order={order} btnClick={handleBtnClick} />
      {/**评论列表 */}
      <Lists lists={comments} showLoading={showLoading} />
      {/**footer */}
      <Footer />
    </div>
  );
}
